/*! \mainpage banker 

\section Documentation

To generate 

\verbatim
mkdir build
cd build
cmake ..
\endverbatim

Doc entry point will be in build/html/index.html


The small code is a BSON parser.

*The api has been taken from https://github.com/mongodb/mongo-cxx-driver.git*

Main idea:
Using the structure of the grammar (bsonspec.org/spec.html) three main classes : Bson, Document, Element.
Bson own the data (vector passed in the constructor. Document and Element classes are only a view of the buffer.

They both have indexes to their sub structure (Bson has a vector of document and Document a vector of Eelement) to have quicker access (bad in the case of a dump better if accessing to element individually).

\section Compiling

\verbatim
mkdir build
cd build
cmake ..
make
make test
\endverbatim

\section Tests
- Using googltest library (which code is embedded in the archive).
- Definitely not complete. Just enough to show how to do them (not enough time sorry).
- Ran the tests with valgrind no memory issues (leaks or corruption)

\section Why using existing api?
- Test compabilities
- I was about to use the same architecture as the grammar (Bson => Document => element...) 
- Would do better in the time I had.

While coding I didn't read any .cpp on the mongocxx driver (only headers)

 


\section What can be improved?
- Test are really light (I mainly wrote some skel to show whan can/should be done)
- Homogeneity of types  (e.g. using struct of bson int32)
- Define a policy when getting the wrong type on element (right now returning default value is implemented for string, and throwing for int32)
- Replacing switch with function vector
  - More readable code
  - More scalable (adding new type keep the main function short) 
something like 
\sectioninclude <vector>
\sectioninclude <iostream>
\sectioninclude <functional>

constexpr int foo (int i, int e){
	return 0;
}

constexpr int bar (int i, int e){
	return i*e;
}


int main(int argc, char *argv[]) {
	std::vector<std::function <int(int,int)> > v(255);
 v[static_cast<int>(Type::Double)] = foo;
 v[static_cast<int>(Type::STRING)] = bar;

	if (argc == 1) {
		std::cout << v[0](1,3) << std::endl;
	} else {
		std::cout << v[1](1,4) << std::endl;
	}
	
	
	return 0;
}

- Implementing all the accessors in tanker::Element
- Use better exception (than std::runtime_error)

*/
