#ifndef TANKER_TYPES_HPP
#define TANKER_TYPES_HPP

#include <climits>
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <vector>

namespace tanker {

using std::int16_t;
using std::int32_t;
using std::int64_t;
using std::int8_t;

using std::uint16_t;
using std::uint32_t;
using std::uint64_t;
using std::uint8_t;

enum class Type {
  DOUBLE = 1,
  STRING = 2,
  DOCUMENT = 3,
  ARRAY = 4,
  BINARY = 5,
  UNDEFINED = 6,
  OBJID = 7,
  BOOL = 8,
  UTC = 9,
  NULL_VAL = 10,
  REGEXP = 11,
  DB_POINTER = 12,
  JS = 13,
  DEPRECATED = 14,
  JS_W_SCOPE = 15,
  INT32 = 16,
  TIMESTAMPS = 17,
  INT64 = 18,
  MAX_KEY = 127,
  MIN_KEY = 255
};

using DocumentType = uint8_t;
using Data = std::vector<char>;
using Oid = const uint8_t *;

///
/// using a buffer return an int32 from a bson regarding of the local endianness
///
int32_t getInt32(const Data::const_iterator &it,
                 const Data::const_iterator &end);

///
/// using a buffer return an int64 from a bson regarding of the local endianness
/// @param it iterator to the beginning of the buffer
/// @param end iterator to the end of the buffer (of the end of the int64)
///
int64_t getInt64(const Data::const_iterator &it,
                 const Data::const_iterator &end);

double getDouble(const Data::const_iterator &it,
                 const Data::const_iterator &end);

///
/// strlen with overflow check
/// @return the position -1 of the first '\0' or -1
///
int32_t safeStrlen(Data::const_iterator it, const Data::const_iterator &end);

///
/// return the size of the data element
/// @param it iterator to the beginning of the buffer
/// @param end iterator to the end of the buffer (of the end of the int64)
/// @return size of the data element or -1 on error
///
int32_t getTypeSize(Type type, const Data::const_iterator &it,
                    const Data::const_iterator &end);

// redefined to avoid using experimental C++1y
class string_view {
 private:
  const char *pData;

 public:
  string_view() : pData(nullptr) {}

  string_view(const Data::const_iterator &it) : pData(&(*it)) {}
  string_view(const char *pData) : pData(pData) {}
  void set(const char *_pData) { pData = _pData; }

  const char *const data() const { return pData; }
  friend std::ostream &operator<<(std::ostream &os, string_view const &m);
  friend bool operator==(const string_view &, const string_view &);
  friend bool operator!=(const string_view &, const string_view &);
};

}  // namespace tanker

#endif /* TANKER_TYPES_HPP */
