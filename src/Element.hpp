#ifndef TANKER_ELEMENT_HPP
#define TANKER_ELEMENT_HPP

#include <iostream>
#include "src/Types.hpp"

namespace tanker {
class Document;

class Element {
 public:
 private:
  Data::const_iterator it;
  Data::const_iterator data;
  Data::const_iterator end;

  Type type;
  string_view e_name;
  uint32_t size;
  uint32_t dataSize;

 public:
  ///
  /// Construct an element with a buffer
  ///
  /// @param it iterator to the beginning of the buffer
  /// @param end iterator to the end of the buffer (not the the end of the
  /// Element)
  ///   End is used to check overflow
  ///
  explicit Element(Data::const_iterator &it, Data::const_iterator &end);

  Type getType() const;
  string_view getKey() const;

  ///
  /// @return return a view of the string if the element is actually a string.
  /// Otherwise returns ""
  string_view getString() const;

  ///
  /// @return the number hold by the element. Throw runtime_error on wrong type
  ///
  int32_t getInt32() const;
  int32_t getInt64() const;
  Document getDocument() const;
  Oid getOid() const;

  int32_t length() const;
  friend std::ostream &operator<<(std::ostream &os, const Element &m);
};

}  // namespace tanker

#endif /* TANKER_ELEMENT_HPP */
