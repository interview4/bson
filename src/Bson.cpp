#include <fstream>
#include <iterator>

#include <stdexcept>
#include "src/Bson.hpp"
#include "src/Document.hpp"
#include "src/Types.hpp"

namespace tanker {

Bson::Bson(const Data &data) : _data(data) {
  auto it = _data.cbegin();
  auto end = _data.cend();

  const int32_t size = getInt32(it, end);

  if (_data.size() > LONG_MAX  // data size is unsigned, bson size is... signed
                              // || size < static_cast<int32_t>(_data.size())
      || size < 5) {
    throw std::runtime_error("data size (" + std::to_string(_data.size()) +
                             ")!= bson internal size (" + std::to_string(size) +
                             ")");
  }

  while (it < end) {
    _documents.emplace_back(it, end);
    const auto &doc = _documents.back();
    it += doc.length();
  }
}

const Bson::Documents &Bson::getDocuments() const { return _documents; }

void Bson::dump() const {
  for (const Document &doc : _documents) {
    std::cout << "{" << doc << std::endl << "}" << std::endl;
  }
}

Data loadFile(const std::string &path) {
  std::ifstream is(path, std::ios::binary);
  if (!is.is_open()) {
    throw std::runtime_error("Could not open file " + path);
  }

  std::vector<char> file(is.tellg());
  file.assign(std::istreambuf_iterator<char>(is),
              std::istreambuf_iterator<char>());

  return file;
}

}  // namespace tanker
