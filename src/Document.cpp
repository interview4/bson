#include "src/Document.hpp"

namespace tanker {

Document::Document(const Data::const_iterator &_it,
                   const Data::const_iterator &_end)
    : it(_it), end(_end) {
  auto dataSize = std::distance(it, end);
  size = getInt32(it, end);

  if (static_cast<int32_t>(dataSize) < size) {
    throw std::runtime_error("remaining size smaller than sizeof (int32)");
  }

  end = it + size - 1;  // -1 for trailling 0 on document
  it += sizeof(size);   // document size field

  while (it < end) {
    elementsByPosition.emplace_back(it, end);
    const auto &element = elementsByPosition.back();
    it += element.length();
  }
}

const Document::Elements &Document::getElements() const {
  return elementsByPosition;
}

std::size_t Document::length() const { return size; }

Document::const_iterator Document::cbegin() const {
  return elementsByPosition.cbegin();
}

Document::const_iterator Document::cend() const {
  return elementsByPosition.cend();
}

std::ostream &operator<<(std::ostream &os, const Document &m) {
  bool first = true;
  for (const auto &element : m.getElements()) {
    if (first) {
      first = false;
    } else {
      os << ", ";
    }
    os << element;
  }

  return os;
}

Document::const_iterator Document::find(string_view key) const {
  for (auto _it = elementsByPosition.cbegin(), _end = elementsByPosition.cend();
       _it != _end; ++_it) {
    if (_it->getKey() == key) {
      return _it;
    }
  }
  return elementsByPosition.cend();
}

Document::const_iterator Document::operator[](uint32_t index) const {
  if (index < 0 || index >= elementsByPosition.size()) {
    return cend();
  }

  return elementsByPosition.cbegin() + index;
}

}  // namespace tanker
