#include <iostream>
#include <src/Bson.hpp>
#include <vector>

void print_usage(const std::string &argv0) {
  std::cerr << "Usage : " << argv0
            << " bson_file key to find in the first document" << std::endl;
}

int main(int argc, char *argv[]) {
  if (argc > 2) {
    print_usage(argv[0]);

    return -1;
  }

  const auto file = tanker::loadFile(argv[1]);
  tanker::Bson bson(file);

  bson.dump();

  if (argc == 2) {
    return 0;
  }
  
  if (bson.getDocuments().size() == 0) {
    std::cerr << "No document found" << std::endl;
    return -1;
  }

  auto firstDocument = bson.getDocuments()[0];

  // iterative search
  {
    std::cout << "iterative search using command line argument" << std::endl;
    auto itElement = firstDocument.find(argv[2]);
    if (itElement == firstDocument.cend()) {
      std::cout << "element not found" << std::endl;
    } else {
      std::cout << *itElement << std::endl;
    }
    std::cout << "--------------------------------------------------"
              << std::endl;
  }

  // by index
  {
    std::cout << "Looking for element with index 2" << std::endl;
    auto itElement = firstDocument[2];
    if (itElement == firstDocument.cend()) {
      std::cout << "element not found" << std::endl;
    } else {
      std::cout << *itElement << std::endl;
    }
    std::cout << "--------------------------------------------------"
              << std::endl;
  }

  // accessors
  {
    std::cout << "printing data from element with \"document\" key"
              << std::endl;
    auto itElement = firstDocument.find("document");
    if (itElement != firstDocument.cend()) {
      auto element = *itElement;
      std::cout << "key: " << element.getKey() << " "
                << "value: " << element.getDocument() << std::endl;
    }
    std::cout << "--------------------------------------------------"
              << std::endl;
  }

  return 0;
}
