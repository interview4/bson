// Copyright 2014 MongoDB Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// trax> most (>90%) of the following code is not mine and is coming from
// https://github.com/mongodb/mongo-cxx-driver.git

#ifndef TANKER_DOCUMENT_HPP
#define TANKER_DOCUMENT_HPP

#include <iostream>
#include <string>
#include <vector>

#include "src/Element.hpp"
#include "src/Types.hpp"

namespace tanker {

class Document {
 public:
  using Elements = std::vector<Element>;
  using const_iterator = Elements::const_iterator;

  ///
  /// Default constructs a Document. The resulting Document will be initialized
  /// to point at an empty BSON document.
  ///
  // Document();

  ///
  /// Constructs a Document from a buffer. The caller is responsible for
  /// ensuring that the lifetime of the resulting Document is a subset of the
  /// buffer's.
  ///
  /// @param data
  ///   A buffer containing a valid BSON document.
  /// @param length
  ///   The size of the buffer, in bytes.
  ///

  ///
  /// Constructs a Document from a buffer. The caller is responsible for
  /// ensuring that the lifetime of the resulting Document is a subset of the
  /// buffer's.
  ///
  /// @param it
  ///   iterator on the beginning of the data struct
  /// @param end
  ///   iterator on the end of the data struct
  /// @param length
  ///   The size of the buffer, in bytes.
  ///
  Document(const Data::const_iterator &it, const Data::const_iterator &end);

  ///
  ///
  ///
  const Elements &getElements() const;

  ///
  /// @returns A const_iterator to the first element of the document.
  ///
  const_iterator cbegin() const;

  ///
  /// @returns A const_iterator to the past-the-end element of the document.
  ///
  const_iterator cend() const;

  ///
  /// @brief overload ostream operator to print document
  ///

  ///
  /// Finds the first element of the document with the provided key. If there is
  /// no such element, the past-the-end iterator will be returned. The runtime
  /// of find() is linear in the length of the document. This method only
  /// searches the top-level document, and will not recurse to any subdocuments.
  ///
  /// @remark In BSON, keys are not required to be unique. If there are multiple
  /// elements with a matching key in the document, the first matching element
  /// from the start will be returned.
  ///
  /// @param key
  ///   The key to search for.
  ///
  /// @return An iterator to the matching element, if found, or the past-the-end
  /// iterator.
  ///

  const_iterator find(string_view key) const;

  ///
  /// Finds a element by its position (index)
  /// such element, the invalid document::element will be returned. The runtime
  /// of operator[] is linear in the length of the document.
  ///
  /// @param key
  ///   The index of the element
  ///
  /// @return The matching element, if found, or the invalid element.
  ///
  const_iterator operator[](uint32_t) const;

  ///
  /// Gets the length of the underlying buffer.
  ///
  /// @remark This is not the number of elements in the document.
  /// To compute the number of elements, use std::distance.
  ///
  /// @return The length of the document, in bytes.
  ///
  std::size_t length() const;

 private:
  Data::const_iterator it;
  Data::const_iterator end;
  Elements elementsByPosition;
  int32_t size;
};

std::ostream &operator<<(std::ostream &os, const Document &m);

}  // namespace tanker

#endif /* TANKER_DOCUMENT_HPP */
