#ifndef TANKER_BSON_HPP
#define TANKER_BSON_HPP

#include <vector>
#include "src/Document.hpp"

namespace tanker {

class Bson {
 public:
  using Documents = std::vector<Document>;

 private:
  const Data &_data;
  Documents _documents;

 public:
  Bson(const std::vector<char> &data);
  const Documents &getDocuments() const;
  void dump() const;
};

using Data = std::vector<char>;

Data loadFile(const std::string &path);

}  // namespace tanker

#endif /* TANKER_BSON_HPP */
