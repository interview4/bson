#include "src/Element.hpp"
#include "src/Document.hpp"
#include "src/Types.hpp"

#include <iomanip>

namespace tanker {

Element::Element(Data::const_iterator &it, Data::const_iterator &end)
    : it(it), data(it), end(end), size(0) {
  auto remainingDataCount = std::distance(it, end);
  if (remainingDataCount <
      2) {  // smallest element is 2 bytes empty e_name + no data
    throw std::runtime_error("bad element remaining size too small " +
                             std::to_string(remainingDataCount));
  }

  type = static_cast<Type>(*(it));

  auto stringLen = safeStrlen(it + 1, end);
  if (stringLen == -1) {
    throw std::runtime_error("Bad key string");
  }
  e_name.set(&(*(it + 1)));

  data = it + 1  // type
         + stringLen + 1;

  size = 1 + stringLen + 1;
  dataSize = getTypeSize(type, data, end);
  size += dataSize;
}

int32_t Element::length() const { return size; }

Type Element::getType() const { return type; }
string_view Element::getKey() const { return e_name; }

string_view Element::getString() const {
  if (type != Type::STRING) {
    return "";
  } else {
    return string_view(&(*(data + 4)));
  }
}

int32_t Element::getInt32() const {
  if (type != Type::INT32) {
    throw std::runtime_error("Element is not an int");
  }
  return tanker::getInt32(data, end);
}

int32_t Element::getInt64() const { return tanker::getInt64(data, end); }

tanker::Document tanker::Element::getDocument() const {
  return Document(data, end);
}

Oid Element::getOid() const { return reinterpret_cast<Oid>(&(*data)); }

std::ostream &operator<<(std::ostream &os, const Element &m) {
  static int tabCount = 0;
  os << std::endl;
  for (int i = 0; i < tabCount; i++) {
    os << '\t';
  }

  // TODO use a vector of function instead of a huge switch (see Readme.txt)

  os << "\"" << m.e_name << "\":";

  switch (m.getType()) {
    case Type::DOUBLE:
      os << getDouble(m.data, m.end);
      break;

    case Type::ARRAY:
    case Type::DOCUMENT: {
      tabCount++;
      os << "{";
      Document d(m.data, m.end);
      os << d << std::endl;
      for (int i = 0; i < tabCount; i++) {
        os << '\t';
      }
      os << "}";
      tabCount--;

      break;
    }

    case Type::BINARY:
      for (auto i = m.data, ie = m.data + m.dataSize; i != ie; ++i) {
        os << std::hex << uint8_t(*i) << " ";
      }
      break;

    case Type::UNDEFINED:
      break;

    case Type::OBJID:
      os << "{\"$oid\":\"";
      for (auto i = m.data, ie = m.data + m.dataSize; i != ie; ++i) {
        os << std::setfill('0') << std::setw(2) << std::hex << (0xFF & (*i));
      }
      os << "\"}";
      break;

    case Type::BOOL:
      if (*(m.data) == 0) {
        os << "false";
      } else {
        os << "true";
      }
      break;

    case Type::NULL_VAL:
      os << "NULL";
      break;

    case Type::REGEXP:
      os << "regexp \"" << &(*m.data) << "\" options "
         << &(*(m.data + safeStrlen(m.data, m.end)));
      break;

    case Type::DB_POINTER:
      os << "deprecated";
      break;

    case Type::JS:
    case Type::DEPRECATED:
    case Type::STRING:
      os << "\"" << string_view(m.data + 4) << "\"";
      break;

    case Type::JS_W_SCOPE:
      os << string_view(m.data + 4)
         << Document(m.data + 4 + safeStrlen(m.data, m.end), m.end);
      break;

    case Type::INT32:
      os << getInt32(m.data, m.end);
      break;

    case Type::TIMESTAMPS:
      os << "time ";
    case Type::UTC:
      os << "UTC ";
    case Type::INT64:
      os << getInt64(m.data, m.end);
      os << getInt64(m.data, m.end);
      break;

    case Type::MAX_KEY:
      os << "min_max";
      break;

    case Type::MIN_KEY:
      os << "min_key";
  }

  return os;
}

}  // namespace tanker
