#include "src/Types.hpp"
#include <cstring>
#include <iostream>

#if IS_BIG_ENDIAN
// yes this is not portable
#include <endian.h>
#endif

namespace tanker {

int32_t getInt32(const Data::const_iterator &it,
                 const Data::const_iterator &end) {
  if (static_cast<uint32_t>(std::distance(it, end)) < sizeof(int32_t)) {
    throw std::runtime_error("remaining size smaller than sizeof (int32)");
  }

#if IS_BIG_ENDIAN
  auto r = le32toh(*reinterpret_cast<const int32_t *>(&(*it)));
#else
  auto r = *reinterpret_cast<const int32_t *>(&(*it));
#endif
  return r;
}

int64_t getInt64(const Data::const_iterator &it,
                 const Data::const_iterator &end) {
  if (static_cast<uint32_t>(std::distance(it, end)) < sizeof(int64_t)) {
    throw std::runtime_error("remaining size smaller than sizeof (int32)");
  }

#if IS_BIG_ENDIAN
  auto r = le64toh(*reinterpret_cast<const int64_t *>(&(*it)));
#else
  auto r = *reinterpret_cast<const int64_t *>(&(*it));
#endif
  return r;
}

double getDouble(const Data::const_iterator &it,
                 const Data::const_iterator &end) {
#if IS_BIG_ENDIAN
#error not implemented
#else
  auto r = *reinterpret_cast<const double *>(&(*it));
#endif
  return r;
}

int32_t safeStrlen(Data::const_iterator it, const Data::const_iterator &end) {
  bool foundZ = false;
  int32_t len = 0;
  while (it != end) {
    if (*it == 0) {
      foundZ = true;
      break;
    }
    ++it;
    ++len;
  }
  if (foundZ) {
    return len;
  } else {
    return -1;
  }
}

int32_t getTypeSize(Type type, const Data::const_iterator &it,
                    const Data::const_iterator &end) {
  int32_t elementSize = -1;

  // TODO use a vector of function
  switch (type) {
    case Type::DOUBLE:
      elementSize = sizeof(int64_t);
      break;

    case Type::STRING:
      elementSize = sizeof(int32_t) + getInt32(it, end);
      break;

    case Type::DOCUMENT:
      elementSize =
          getInt32(it, end);  // TODO this is duplicate of Document::length
      break;

    case Type::ARRAY:
      elementSize =
          getInt32(it, end);  // TODO this is duplicate of Document::length
      break;

    case Type::BINARY:
      elementSize = getInt32(it, end);
      break;

    case Type::UNDEFINED:
      elementSize = 0;
      break;

    case Type::OBJID:
      elementSize = 12;
      break;

    case Type::BOOL:
      elementSize = 1;
      break;

    case Type::UTC:
      elementSize = sizeof(int64_t);
      break;

    case Type::NULL_VAL:
      elementSize = 0;
      break;

    case Type::REGEXP:
      elementSize = safeStrlen(it, end);
      elementSize += safeStrlen(it + elementSize, end);
      break;

    case Type::DB_POINTER:
      elementSize = safeStrlen(it, end) + 12;
      break;

    case Type::JS:
      elementSize = getInt32(it, end);
      break;

    case Type::DEPRECATED:
      elementSize = getInt32(it, end);
      break;

    case Type::JS_W_SCOPE:
      elementSize = getInt32(it, end);
      break;

    case Type::INT32:
      elementSize = sizeof(int32_t);
      break;

    case Type::TIMESTAMPS:
      elementSize = sizeof(int64_t);
      break;

    case Type::INT64:
      elementSize = sizeof(int64_t);
      break;

    case Type::MAX_KEY:
      elementSize = 0;
      break;

    case Type::MIN_KEY:
      elementSize = 0;
      break;
  }

  return elementSize;
}

std::ostream &operator<<(std::ostream &os, string_view const &m) {
  return os << m.data();
}

bool operator==(const string_view &a, const string_view &b) {
  return (!strcmp(a.data(), b.data())) ? true : false;
}

bool operator!=(const string_view &a, const string_view &b) {
  return (strcmp(a.data(), b.data())) ? true : false;
}

}  // namespace tanker
