
This code has been written for a test asked by tanker, hence the namespace. 

# build 

``` 
mkdir build 
cd build 
cmake .. 
make 
```

# run 

Dump sson

```
cd build/src
./main file.sson 
```


Dump sson and find document 

```
cd build/src
./main file.sson key
```

Tests 

```
cd build
ctest 
```


# Documentation 

Is generated during build operation (doxygen).
Can be found in `./build/html/index.html`


# samples 

```
for sson in ~/Downloads/sson_exercise-master/sample/*sson ; do ./main $sson ; done
{
}
{
"Question":"Meaning of life?", 
"Answer":42
}
{
"Questions":{
	"0":"What is a linear recurrence?", 
	"1":"Which of the following are?", 
	"2":"What are others?"
	}, 
"First":{
	"0":0, 
	"1":1, 
	"2":1, 
	"3":2, 
	"4":3, 
	"5":5, 
	"6":8, 
	"7":13, 
	"8":21, 
	"9":34, 
	"10":55, 
	"11":89, 
	"12":144
	}, 
"Second":{
	"0":1, 
	"1":11, 
	"2":21, 
	"3":1211, 
	"4":111221, 
	"5":312211, 
	"6":13112221, 
	"7":1113213211
	}, 
"Third":{
	"0":4, 
	"1":8, 
	"2":15, 
	"3":16, 
	"4":23, 
	"5":42
	}
}
{
"Ça peut t'intéresser.":{
	"0":{
		}, 
	"1":{
		"Question":"Meaning of life?", 
		"Answer":42
		}, 
	"2":{
		"Questions":{
			"0":"What is a linear recurrence?", 
			"1":"Which of the following are?", 
			"2":"What are others?", 
			"3":"どういう意味ですか。"
			}, 
		"First":{
			"0":0, 
			"1":1, 
			"2":1, 
			"3":2, 
			"4":3, 
			"5":5, 
			"6":8, 
			"7":13, 
			"8":21, 
			"9":34, 
			"10":55, 
			"11":89, 
			"12":144
			}, 
		"Second":{
			"0":1, 
			"1":11, 
			"2":21, 
			"3":1211, 
			"4":111221, 
			"5":312211, 
			"6":13112221, 
			"7":1113213211
			}, 
		"Third":{
			"0":4, 
			"1":8, 
			"2":15, 
			"3":16, 
			"4":23, 
			"5":42
			}
		}
	}, 
"Aha":{
	"0":{
		}, 
	"1":{
		}, 
	"2":{
		}
	}
}
``` 


Vs reference 

```
for sson in ~/Downloads/sson_exercise-master/sample/*sson ; do bsondump $sson ; done
{}
2019-12-04T17:38:33.848+0100	1 objects found
{"Question":"Meaning of life?","Answer":42}
2019-12-04T17:38:33.858+0100	1 objects found
{"Questions":["What is a linear recurrence?","Which of the following are?","What are others?"],"First":[0,1,1,2,3,5,8,13,21,34,55,89,144],"Second":[1,11,21,1211,111221,312211,13112221,1113213211],"Third":[4,8,15,16,23,42]}
2019-12-04T17:38:33.867+0100	1 objects found
{"Ça peut t'intéresser.":[{},{"Question":"Meaning of life?","Answer":42},{"Questions":["What is a linear recurrence?","Which of the following are?","What are others?","どういう意味ですか。"],"First":[0,1,1,2,3,5,8,13,21,34,55,89,144],"Second":[1,11,21,1211,111221,312211,13112221,1113213211],"Third":[4,8,15,16,23,42]}],"Aha":[{},{},{}]}
2019-12-04T17:38:33.874+0100	1 objects found
```
