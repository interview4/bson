#include "gtest/gtest.h"
#include "src/Bson.hpp"

TEST(Element, accessors) {  //{"foo":"bar"}
  try {
    auto file = tanker::loadFile("bsons/testElement.bson");
    tanker::Bson b(file);

    auto firstDocument = b.getDocuments()[0];

    {
      auto itElement = firstDocument.find("string");
      ASSERT_NE(itElement, firstDocument.cend()) << "Eleement not found";
      auto element = *itElement;
      ASSERT_STREQ(element.getKey().data(), "string") << "key should be ==";
      ASSERT_STREQ(element.getString().data(), "is a string")
          << "getString not working";
    }
    {
      auto itElement = firstDocument.find("int32");
      ASSERT_NE(itElement, firstDocument.cend()) << "Eleement not found";
      auto element = *itElement;
      ASSERT_EQ(element.getInt32(), 42) << "getInt32 not working";
    }

  } catch (std::exception &e) {
    ASSERT_FALSE(true) << "should not throw " << e.what();
  }
}

int main(int argc, char *argv[]) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
