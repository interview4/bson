#include "src/Bson.hpp"
#include "gtest/gtest.h"

TEST(Bson, loadFile) {
  bool didThrow = false;
  // load file fails
  try {
    auto v = tanker::loadFile("/proc/foo");
  } catch (std::exception &e) {
    didThrow = true;
  }

  // load file succeed
  didThrow = false;
  try {
    auto v = tanker::loadFile("bsons/bson_wrong_size.bson");
  } catch (std::exception &e) {
    didThrow = true;
  }

  ASSERT_FALSE(didThrow);
}

TEST(Bson, checkSizeFail) {
  bool didThrow = false;
  auto v = tanker::loadFile("bsons/bson_wrong_size.bson");
  try {
    tanker::Bson b(v);

  } catch (std::exception &e) {
    didThrow = true;
  }

  ASSERT_TRUE(didThrow);
}

TEST(Bson, checkSizeSucceed) {
  bool didThrow = false;
  try {
    tanker::Bson b(tanker::loadFile("bsons/bson_right_size.bson"));
  } catch (std::exception &e) {
    didThrow = true;
    std::cout << e.what() << std::endl;
  }

  ASSERT_FALSE(didThrow);
}

TEST(Bson, empty) {  //{}
  try {
    tanker::Bson b(tanker::loadFile("bsons/0.bson"));
    auto docs = b.getDocuments();
    ASSERT_EQ(docs.size(), 1u);

    auto elements = docs[0].getElements();
    ASSERT_EQ(elements.size(), 0u);

  } catch (std::exception &e) {
    ASSERT_FALSE(true) << "empty bson should not be an issue";
  }
}

TEST(Bson, trivial) {  //{"foo":"bar"}
  try {
    auto file = tanker::loadFile("bsons/1.bson");
    tanker::Bson b(file);
    auto docs = b.getDocuments();
    ASSERT_EQ(docs.size(), 1u);

    auto elements = docs[0].getElements();
    ASSERT_EQ(elements.size(), 1u);

    ASSERT_STREQ(elements[0].getKey().data(), "foo");

  } catch (std::exception &e) {
    ASSERT_FALSE(true) << "should not throw " << e.what();
  }
}

TEST(Bson, providedTest0) {  //{"foo":"bar"}
  try {
    auto file = tanker::loadFile("bsons/test.bson");
    tanker::Bson b(file);
    auto docs = b.getDocuments();
    ASSERT_EQ(docs.size(), 1u);

    auto elements = docs[0].getElements();
    ASSERT_EQ(elements.size(), 2u);

    ASSERT_STREQ(elements[0].getKey().data(), "_id");

  } catch (std::exception &e) {
    ASSERT_FALSE(true) << "should not throw " << e.what();
  }
}

TEST(Bson, providedTest1) {
  try {
    auto file = tanker::loadFile("bsons/mongodump.airpair.tags.bson");
    tanker::Bson b(file);
    auto docs = b.getDocuments();
    ASSERT_EQ(docs.size(), 50u);

    auto elements = docs[0].getElements();
    ASSERT_EQ(elements.size(), 8u);

    ASSERT_STREQ(elements[0].getKey().data(), "_id");

  } catch (std::exception &e) {
    ASSERT_FALSE(true) << "should not throw " << e.what();
  }
}

int main(int argc, char *argv[]) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
